<html lang="en">
<head>
  <title>Admin Site</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<style media="screen">
.navbar-default{
	background-color: #006699;
}
.navbar-default .navbar-brand{
    color:white;
    font-family: arial black;
}
.navbar-default .navbar-brand:hover,
.navbar-default .navbar-brand:focus{
    color:white;
}
.navbar-default .navbar-nav > li > a{
    color:white;
    font-family: arial black;
}
.navbar-default .navbar-nav > li >a:active,
.navbar-default .navbar-nav >li > a:hover,
.navbar-default .navbar-nav >li>a:focus{
    background-color: white;
}   
.sidenav {
      background-color: snow;
      border-right-color: black;
      border-style:double;
}
</style>
</head>
<body>
<div class="navbar navbar-default navbar-fixed-top col-xs-12 col-md-12 col-lg-12" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header ">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>  
                    <span class="icon-bar"></span> 
                    <span class="icon-bar"></span> 
             </button>
            <a class="navbar-brand" href="admin.php">IGWA KITA Admin Site</a>
        </div>
    <div class="navbar-collapse collapse">
        <form action="index.php" method="POST">
         <ul class="nav navbar-nav navbar-right">
            <li><a href="admin.php">Home</a></li>    
            <li><a href="About.php">About</a></li>
            <li><button class="btn btn-danger navbar-btn" name="logout">Log out</button></li>
        </form>
      </div>
    </div>
</div>
<div class="container-fluid" style="margin-top:3%;">
  <div class="row admimage content">
    <div class="col-sm-2 sidenav"  style="margin-top:5.5%;">
      <h1>Categories</h1>
      <ul class="nav nav-pills nav-stacked">
        <li style="font-size:18px; color:black;font-family:Arial Black">Skilled Labor</li>
        <li class="active"><a href="adkarpintero.php" style="color:black;">Karpintero</a></li> 
        <li><a href="#" style="color:black;">Tubero</a></li> 
        <li><a href="#" style="color:black;">Mananahi</a></li> 
        <li><a href="#" style="color:black;">Pintor</a></li>
        <li><a href="#" style="color:black;">Mekaniko</a></li>
        <li><a href="#" style="color:black;">Welder</a></li> 
        <li><a href="#" style="color:black;">Electrician</a></li>
        <li style="font-size:18px; color:black; font-family:Arial Black">Tutorials</li>
        <li><a href="#" style="color:black;">Math</a></li>
        <li><a href="#" style="color:black;">Science</a></li> 
        <li><a href="#" style="color:black;">Accounting</a></li> 
        <li><a href="#" style="color:black;">Arts</a></li>
        <li><a href="#" style="color:black;">Music</a></li>
        <li><a href="#" style="color:black;">Swimming</a></li> 
        <li><a href="#" style="color:black;">Martial Arts</a></li>
        <li style="font-size:18px; color:black; font-family:Arial Black">Rare Services</li>
        <li><a href="#" style="color:black;">Manghihilot</a></li>
        <li><a href="#" style="color:black;">Santigwar</a></li> 
        <li><a href="#" style="color:black;">Manghuhula</a></li> 
      </ul>
    </div>
    <div class="col-sm-10">
        <h1 class="h1 text-center">Karpintero Data</h1>
		<div class="table-responsive">
            <table class="table table-bordered">
                <form action="adkarpintero.php" method="POST">
                <thead style="background-color: #006699; color:white;">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Firsname</th>
                    <th scope="col">Lastname</th>
                    <th scope="col">Age</th>
                    <th scope="col">Address</th>
                    <th scope="col">Occupation</th>
                    <th scope="col">Availiability</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
            <tbody>
                <tr>
                      <th scope='row'></th>
                      <td><input class='text-box'type='text' name ='fname' placeholder="Firstname"></td>
                      <td><input class='text-box'type='text' name ='lname' placeholder="Lastname"></td>
                      <td><input class='text-box'type='text' name ='age' placeholder="Age"></td>
                      <td><input class='text-box'type='text' name ='address' placeholder="Address"></td>
                      <td><input class='text-box'type='text' name ='occupation' placeholder="Occupation"></td>
                      <td><select name='status'><option value="Available">Available</option>
								              <option value="NotAvailable">Not Available</option></select></td>
                      <td><button class="btn btn-default" type="submit" name="save">Save</button></td>
                      </tr>
                </form>
                <?php
                  $value = 'Karpintero';
                  $con = mysqli_connect("localhost","root","","localwebdb");
                  $query = mysqli_query($con ,"SELECT * FROM karpintero_tbl WHERE CONCAT(id,fname,lname,age,address,occupation,availability) LIKE '%".$value."%'");
				  $count = mysqli_num_rows($query);

				if(isset($_POST['save']))
                    {
                    session_start();
                    $fname = mysqli_real_escape_string($con,$_POST['fname']);
                    $lname = mysqli_real_escape_string($con,$_POST['lname']);
                    $age = mysqli_real_escape_string($con,$_POST['age']);
                    $address = mysqli_real_escape_string($con,$_POST['address']);
                    $occupation = mysqli_real_escape_string($con,$_POST['occupation']);
                    $status = mysqli_real_escape_string($con,$_POST['status']);

                    $query = "SELECT * FROM karpintero_tbl where fname='$fname' and lname='$lname'" ;
                    $res = mysqli_query($con,$query);
                    $count = mysqli_num_rows($res);
                if (($_POST['occupation'] == $value) && ($count==0))
                    {
                    $query2 = "INSERT INTO karpintero_tbl VALUES('','$fname','$lname','$age','$address','$occupation','$status')";
                    mysqli_query($con,$query2);
                    echo '<script language="javascript">';
                    echo 'alert("successfully Added!")
                     window.location.replace("adkarpintero.php")
                    </script>';
                    }
                else
                    {
                    echo "error".myqli_error();
                    }
                }
                if($count>=1){
                    while($row = mysqli_fetch_array($query)){
                    ?>
                      <form action='adkarpintero.php' method="POST">
                      <tr>
                      <th scope='row'><?php echo $row['id']; ?><input type='hidden' name ='id' value="<?php echo $row['id']; ?>"></td></th>
                      <td><input class="text-box" type="text" name ="fname" value="<?php echo $row['fname']; ?>"</td>
                      <td><input class='text-box'type='text' name ='lname' value="<?php echo $row['lname']; ?>"</td>
                      <td><input class='text-box'type='text' name ='age' value="<?php echo $row['age']; ?>"</td>
                      <td><input class='text-box'type='text' name ='address' value="<?php echo $row['address']; ?>"</td>
                      <td><?php echo $row['occupation']; ?></td>
                      <td><?php echo $row['availability']; ?><select name="status"><option value="Available">Available</option>
								              <option value="NotAvailable">Not Available</option></select></td>
                        <td><button class='btn btn-default' type='submit' name='edit'>Update</button>
						    <button class='btn btn-default' type='submit' name='delete'>Delete</button></td>
                      </tr>
                      </form>
             <?php
                    }
                }
                if(isset($_POST['edit']))
				{
					$fname = mysqli_real_escape_string($con,$_POST['fname']);
                    $lname = mysqli_real_escape_string($con,$_POST['lname']);
                    $age = mysqli_real_escape_string($con,$_POST['age']);
                    $address = mysqli_real_escape_string($con,$_POST['address']);
                    $occupation = 'Karpintero';
                    $status = mysqli_real_escape_string($con,$_POST['status']);
					$uquery ="UPDATE karpintero_tbl SET fname='$fname',lname='$lname',age='$age',address='$address',occupation='$occupation',availability='$status' WHERE id='$_POST[id]'";
						mysqli_query($con,$uquery);
						if ($uquery)
						{
							echo "<script type='text/javascript'>window.location.replace('adkarpintero.php')</script>";
						}
						else
						{
							echo "error".myqli_error();
						}
				}
                if(isset($_POST['delete']))
				{
						$dquery ="DELETE FROM karpintero_tbl WHERE id='$_POST[id]'";
						mysqli_query($con,$dquery);
						if ($dquery)
						{
							echo "<script type='text/javascript'>window.location.replace('adkarpintero.php')</script>";
						}
						else
						{
							echo "error".myqli_error();
						}
				}
                if(isset($_post['logout']))
                {
                    session_start();
                    session_destroy();
                    header("location: index.php");
                    exit;
                }
				?>
            </tbody>
            </table>
		</div>
    </div>
  </div>
</div>
</body>
</html>