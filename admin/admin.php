<html lang="en">
<head>
  <title>Admin Site</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<style media="screen">
.navbar-default{
	background-color: #006699;
}
.navbar-default .navbar-brand{
    color:white;
    font-family: arial black;
}
.navbar-default .navbar-brand:hover,
.navbar-default .navbar-brand:focus{
    color:white;
}
.navbar-default .navbar-nav > li > a{
    color:white;
    font-family: arial black;
}
.navbar-default .navbar-nav > li >a:active,
.navbar-default .navbar-nav >li > a:hover,
.navbar-default .navbar-nav >li>a:focus{
    background-color: white;
}   
.sidenav {
      background-color: snow;
      border-right-color: black;
      border-style:double;
}
.admimage{
        background-image: url(admimg/Mayon.jpg);
        background-size:cover;
        background-position: center;
        background-repeat: no-repeat;
}
.h1{
        margin-top:45vh;
        font-size: 450%;
        color: #003366;
        position:relative;
        animation: text 3s 1;
}
     @keyframes text{
         0%{
             color:white;
             margin-bottom: -40px;
         }
         50%{
             letter-spacing: 65px;
             margin-bottom: -40px;
         }
                
}
</style>
</head>
<body>
<div class="navbar navbar-default navbar-fixed-top col-xs-12 col-md-12 col-lg-12" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header ">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>  
                    <span class="icon-bar"></span> 
                    <span class="icon-bar"></span> 
             </button>
            <a class="navbar-brand" href="admin.php">IGWA KITA Admin Site</a>
        </div>
    <div class="navbar-collapse collapse">
        <form action="index.php" method="POST">
         <ul class="nav navbar-nav navbar-right">
            <li><a href="admin.php" style="background-color:white; color:black;">Home</a></li>    
            <li><a href="About.php">About</a></li>  
            <li><button class="btn btn-danger navbar-btn" name="logout">Log out</button></li>
        </form>
      </div>
    </div>
</div>
<div class="container-fluid" style="margin-top:3%;">
  <div class="row admimage content">
    <div class="col-sm-3 sidenav"  style="margin-top:5.5%;">
      <h1>Categories</h1>
      <ul class="nav nav-pills nav-stacked">
        <li style="font-size:18px; color:black;font-family:Arial Black">Skilled Labor</li>
        <li><a href="adkarpintero.php" style="color:black;">Karpintero</a></li> 
        <li><a href="#" style="color:black;">Tubero</a></li> 
        <li><a href="#" style="color:black;">Mananahi</a></li> 
        <li><a href="#" style="color:black;">Pintor</a></li>
        <li><a href="#" style="color:black;">Mekaniko</a></li>
        <li><a href="#" style="color:black;">Welder</a></li> 
        <li><a href="#" style="color:black;">Electrician</a></li>
        <li style="font-size:18px; color:black; font-family:Arial Black">Tutorials</li>
        <li><a href="#" style="color:black;">Math</a></li>
        <li><a href="#" style="color:black;">Science</a></li> 
        <li><a href="#" style="color:black;">Accounting</a></li> 
        <li><a href="#" style="color:black;">Arts</a></li>
        <li><a href="#" style="color:black;">Music</a></li>
        <li><a href="#" style="color:black;">Swimming</a></li> 
        <li><a href="#" style="color:black;">Martial Arts</a></li>
        <li style="font-size:18px; color:black; font-family:Arial Black">Rare Services</li>
        <li><a href="#" style="color:black;">Manghihilot</a></li>
        <li><a href="#" style="color:black;">Santigwar</a></li> 
        <li><a href="#" style="color:black;">Manghuhula</a></li> 
      </ul>
    </div>
    <div class="col-sm-9">
        <h1 class="h1 text-center">IGWA KITA</h1>
    </div>
  </div>
</div>
<?php
    if(isset($_post['logout']))
       {
           session_start();
           session_destroy();
           header("location: index.php");
           exit;
        }
				
?>
</body>
</html>