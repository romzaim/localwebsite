!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<style media="screen">
.navbar-default{
	background-color: #006699;
}
.navbar-default .navbar-brand{
    color:white;
    font-family: arial black;
}
.navbar-default .navbar-brand:hover,
.navbar-default .navbar-brand:focus{
    color:white;
}

.navbar-default .navbar-nav > li > a{
    color:white;
    font-family: arial black;
}
.navbar-default .navbar-nav > li >a:active,
.navbar-default .navbar-nav >li > a:hover,
.navbar-default .navbar-nav >li>a:focus{
    background-color: white;
}
.dropdown-menu > li>a{
    background-color: white;
}
.dropdown-menu > li>a:hover{
    background-color: skyblue;     
}  
.navbar-default .navbar-header > .navbar-toggle:focus{
        background-color: #006699;
}
.navbar-default .navbar-header > .navbar-toggle:hover{
		background-color: skyblue;
}
.h1{
       margin-top: 60px; 
}
footer {
      background-color: #555;
      color: white;
      padding: 15px;
}
</style>
</head>
<body>
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>  
                    <span class="icon-bar"></span> 
                    <span class="icon-bar"></span> 
                </button>
                <a class="navbar-brand" href="index.html">WebsiteName</a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="index.html">Home</a></li>    
                    <li class="active"><a href="#" style="background-color:white; color:black;">About</a></li>  
                    <li><a href="Contact.php">Contact</a></li>
                    <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Categories<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li class="dropdown-header" style="font-size:18px; color:black;font-family:Arial Black">Skilled Labor</li>
                            <li><a href="Karpintero.php" style="color:black;">Karpintero</a></li> 
                            <li><a href="#" style="color:black;">Tubero</a></li> 
                            <li><a href="#" style="color:black;">Mananahi</a></li> 
                            <li><a href="#" style="color:black;">Pintor</a></li>
                            <li><a href="#" style="color:black;">Mekaniko</a></li>
                            <li><a href="#" style="color:black;">Welder</a></li> 
                            <li><a href="#" style="color:black;">Electrician</a></li>
                            <li class="dropdown-header" style="font-size:18px; color:black; font-family:Arial Black">Tutorials</li>
                            <li><a href="#" style="color:black;">Math</a></li>
                            <li><a href="#" style="color:black;">Science</a></li> 
                            <li><a href="#" style="color:black;">Accounting</a></li> 
                            <li><a href="#" style="color:black;">Arts</a></li>
                            <li><a href="#" style="color:black;">Music</a></li>
                            <li><a href="#" style="color:black;">Swimming</a></li> 
                            <li><a href="#" style="color:black;">Aartial Arts</a></li>
                            <li class="dropdown-header" style="font-size:18px; color:black; font-family:Arial Black">Rare Services</li>
                            <li><a href="#" style="color:black;">Manghihilot</a></li>
                            <li><a href="#" style="color:black;">Santigwar</a></li> 
                            <li><a href="#" style="color:black;">Manghuhula</a></li> 
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
<div class="row bground">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="container-fluid">
            <h1 class="h1 text-center">About</h1>
		</div>
	</div>
</div>
<footer class="container-fluid text-center">
  <p>Footer Text</p>
</footer>
</body>
</html>