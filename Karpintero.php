<html lang="en">
<head>
  <title>Karpintero Site</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<style media="screen">
.navbar-default{
	background-color: #006699;
}
.navbar-default .navbar-brand{
    color:white;
    font-family: arial black;
}
.navbar-default .navbar-brand:hover,
.navbar-default .navbar-brand:focus{
    color:white;
}

.navbar-default .navbar-nav > li > a{
    color:white;
    font-family: arial black;
}
.navbar-default .navbar-nav > li >a:active,
.navbar-default .navbar-nav >li > a:hover,
.navbar-default .navbar-nav >li>a:focus{
    background-color: white;
}
.dropdown-menu > li>a{
    background-color: white;
}  
.navbar-default .navbar-header > .navbar-toggle:focus{
        background-color: #006699;
}
.navbar-default .navbar-header >{
		background-color: skyblue;
}
.h1{
       margin-top: 60px;
}
footer {
      background-color: #555;
      color: white;
      padding: 15px;
}    
/* Extra small devices (phones, 600px and down) */
@media only screen and (max-width: 600px) {
}

/* Small devices (portrait tablets and large phones, 600px and up) */
@media only screen and (min-width: 600px) {

}

/* Medium devices (landscape tablets, 768px and up) */
@media only screen and (min-width: 768px) {

} 

/* Large devices (laptops/desktops, 992px and up) */
@media only screen and (min-width: 992px) {

} 

/* Extra large devices (large laptops and desktops, 1200px and up) */
@media only screen and (min-width: 1200px) {
}
</style>
</head>
<body>
    <div class="navbar navbar-default navbar-fixed-top col-xs-12 col-md-12 col-lg-12" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>  
                    <span class="icon-bar"></span> 
                    <span class="icon-bar"></span> 
                </button>
                <a class="navbar-brand" href="index.html">WebsiteName</a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="index.html">Home</a></li>    
                    <li><a href="#">About</a></li>  
                    <li><a href="Contact.php">Contact</a></li>
                    <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="background-color:white; color:black;">Categories<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li class="dropdown-header" style="font-size:18px; color:black;font-family:Arial Black">Skilled Labor</li>
                            <li><a href="#" style="color:black; background-color: skyblue">Karpintero</a></li> 
                            <li><a href="#" style="color:black;">Tubero</a></li> 
                            <li><a href="#" style="color:black;">Mananahi</a></li> 
                            <li><a href="#" style="color:black;">Pintor</a></li>
                            <li><a href="#" style="color:black;">Mekaniko</a></li>
                            <li><a href="#" style="color:black;">Welder</a></li> 
                            <li><a href="#" style="color:black;">Electrician</a></li>
                            <li class="dropdown-header" style="font-size:18px; color:black; font-family:Arial Black">Tutorials</li>
                            <li><a href="#" style="color:black;">Math</a></li>
                            <li><a href="#" style="color:black;">Science</a></li> 
                            <li><a href="#" style="color:black;">Accounting</a></li> 
                            <li><a href="#" style="color:black;">Arts</a></li>
                            <li><a href="#" style="color:black;">Music</a></li>
                            <li><a href="#" style="color:black;">Swimming</a></li> 
                            <li><a href="#" style="color:black;">Martial Arts</a></li>
                            <li class="dropdown-header" style="font-size:18px; color:black; font-family:Arial Black">Rare Services</li>
                            <li><a href="#" style="color:black;">Manghihilot</a></li>
                            <li><a href="#" style="color:black;">Santigwar</a></li> 
                            <li><a href="#" style="color:black;">Manghuhula</a></li> 
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
<div class="row">
	<div class="container-fluid">
        <h1 class="h1 text-center">Karpintero Data</h1>
		<div class="table-responsive">
            <table class="table table-bordered">
                <thead style="background-color: #006699; color:white;">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Age</th>
                    <th scope="col">Address</th>
                    <th scope="col">Occupation</th>
                    <th scope="col">Availiability</th>
                </tr>
                </thead>
            <tbody>
                <?php
                $value = 'Karpintero';
                  $con = mysqli_connect("localhost","root","","localwebdb");
                  $query = mysqli_query($con ,"SELECT * FROM karpintero_tbl WHERE CONCAT(id,name,age,address,occupation,availability) LIKE '%".$value."%'");
				  $count = mysqli_num_rows($query);
                if($count>=1){
                    while($row = mysqli_fetch_array($query)){
                    echo "<tr>
                      <th scope='row'>".$row['id']."</th>
                      <td>".$row['name']."</td>
                      <td>".$row['age']."</td>
                      <td>".$row['address']."</td>
                      <td>".$row['occupation']."</td>
                      <td>".$row['availability']."</td>
                      </tr>";
                    }
                }
                ?>
            </tbody>
            </table>
		</div>
	</div>
</div>
<footer class="container-fluid text-center">
  <p>Footer Text</p>
</footer>
</body>
</html>